package com.foo.test.shirotest1.security.entity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PUConfigurationHBPropUpdateAndCreateTest.class, PUConfigurationJavaxPropCreateTest.class,
		PUConfigurationJavaxPropDllGenerationTest.class, PUConfigurationJavaxPropDropAndCreateTest.class,
		PUConfigurationJavaxPropDropAndCreateWithScriptTest.class })
public class AllJPAPUConfigTest {

}
