package com.foo.test.shirotest1.security.entity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ RoleAndUserTest.class, RoleTest.class, UserTest.class })
public class AllJPAEntitiesTest {

}
