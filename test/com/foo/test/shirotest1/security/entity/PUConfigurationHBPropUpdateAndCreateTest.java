package com.foo.test.shirotest1.security.entity;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PUConfigurationHBPropUpdateAndCreateTest {
	private static EntityManagerFactory emfHBUpdate;
	private static EntityManagerFactory emfHBCreate;
	private static int HBUdateCount;

	@BeforeClass
    public static void beforClass() {
		emfHBUpdate = Persistence.createEntityManagerFactory( "shiroJPUTest-Local-HB-Propierties-update" );
		emfHBCreate = Persistence.createEntityManagerFactory( "shiroJPUTest-Local-HB-Propierties-create" );
		
		assertEquals("update", emfHBUpdate.getProperties().get("hibernate.hbm2ddl.auto") );
		assertEquals("create", emfHBCreate.getProperties().get("hibernate.hbm2ddl.auto") );
		
		HBUdateCount = HbUpdate_StartCount();
		System.out.println("*** PUConfigurationTest.beforClass(), HBUdateCount= " + HBUdateCount);
	}

	@AfterClass
    public static void afterClass() {
		emfHBUpdate.close();
		emfHBCreate.close();
	}
	
    private static int HbUpdate_StartCount()  {
        EntityManager em = emfHBUpdate.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        em.close();
        return lu.size();
    }
	
	
	@Before
    public void beforTest() {
		emfHBUpdate = Persistence.createEntityManagerFactory( "shiroJPUTest-Local-HB-Propierties-update" );
		emfHBCreate = Persistence.createEntityManagerFactory( "shiroJPUTest-Local-HB-Propierties-create" );
	}

    @After
    public void afterTest() {
    	emfHBUpdate.close();
    	emfHBCreate.close();
    }

    public void insert(EntityManager em, int count) {
        em.getTransaction().begin();
        
        for (int i = 0; i < count; i++)
        	em.persist(new User("Nombre" + i, "Pass" + i));

        em.getTransaction().commit();
    }
    
    
    
    @Test
    public void withHbCreate_InsertTwo() throws Exception {
        EntityManager em = emfHBCreate.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(0, lu.size());

        insert(em, 2);

        em.close();
    	
    }

    @Test
    public void withHbCreate_InsertThree() throws Exception {
        EntityManager em = emfHBCreate.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(0, lu.size());

        insert(em, 3);

        em.close();
    }
    
    @Test
    public void withHbUpdate_InsertTwo() throws Exception {
        EntityManager em = emfHBUpdate.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(HBUdateCount, lu.size());
        System.out.println("*** PUConfigurationTest.withHbUpdate_InsertTwo(), HBUdateCount = " + HBUdateCount);

        insert(em, 2);

        em.close();
        HBUdateCount +=2;
    	
    }

    @Test
    public void withHbUpdate_InsertTwoAgain() throws Exception {
        EntityManager em = emfHBUpdate.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(HBUdateCount, lu.size());
        System.out.println("*** PUConfigurationTest.withHbUpdate_InsertTwo(), HBUdateCount = " + HBUdateCount);
        
        insert(em, 2);

        em.close();
        HBUdateCount +=2;
    }

    @Test
    public void withHbUpdate_InsertOne() throws Exception {
        EntityManager em = emfHBUpdate.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(HBUdateCount, lu.size());
        System.out.println("*** PUConfigurationTest.withHbUpdate_InsertTwo(), HBUdateCount = " + HBUdateCount);
        
        insert(em, 1);

        em.close();
        HBUdateCount +=1;
    }
    
    @Test
    public void withHbCreate_count() throws Exception {
        EntityManager em = emfHBCreate.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(0, lu.size());

        em.close();
    }

    @Test
    public void withHbUpdate_count() throws Exception {
        EntityManager em = emfHBUpdate.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        System.out.println("*** PUConfigurationTest.withHbCreate_count(), HBCreateeCount = " + HBUdateCount);
        assertEquals(HBUdateCount, lu.size());

        em.close();
    }
    
}
