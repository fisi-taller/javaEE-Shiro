package com.foo.test.shirotest1.security.entity;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.OptimisticLockException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserTest {
	private static EntityManagerFactory entityManagerFactory;

	
	private static void loadSpecificPUFile(String puFile){
		Thread.currentThread().setContextClassLoader(new ClassLoader() {
	        @Override
	        public Enumeration<URL> getResources(String name) throws IOException {
	            if (name.equals("META-INF/persistence.xml")) {
	                return Collections.enumeration(Arrays.asList(new File(puFile)
	                        .toURI().toURL()));
	            }
	            return super.getResources(name);
	        }
	    });
	}
	
	@BeforeClass
	public static void beforeClass() {
		System.out.println("*****UserTest.beforeClass()");
		loadSpecificPUFile("test-resources/META-INF/persistenceTest.xml");
        //entityManagerFactory = Persistence.createEntityManagerFactory( "shiroJPU" );
		
        entityManagerFactory = Persistence.createEntityManagerFactory( "PUTest1" );
	}
	@AfterClass
	public static void afterClass() {
		System.out.println("*****UserTest.afterClass()");
        entityManagerFactory.close();
	}

	/*
	@Before
    public void beforTest() {
		System.out.println("*****UserTest.init()");
		//Si las inciaciones se hace aqui
		//entityManagerFactory no tiene que ser static
		//entityManagerFactory = Persistence.createEntityManagerFactory( "shiroJPU" );
        entityManagerFactory = Persistence.createEntityManagerFactory( "shiroJPUTest-Local" );

	}

    @After
    public void afterTest() {
    	System.out.println("*****UserTest.destroy()");
        entityManagerFactory.close();
    }
    */
    
    @Test
    public void afterInsertShouldGetDifferentState() throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        
        User u1 = new User("NombreX1", "PassX1");

        assertEquals(0, u1.getId());
        assertNull(u1.getVersion());
        
        entityManager.persist(u1);

        assertNotEquals(0, u1.getId());
        assertNotNull(u1.getVersion());
        
        System.out.println("UserTest.afterInsertShouldGetDifferentState(): u1=" + u1);
        
        User u = entityManager.find(User.class, u1.getId());
        assertEquals(u, u1);
        
        entityManager.getTransaction().commit();
        entityManager.close();
    }
    
    @Test
    public void twoEntityShouldGetDifferentID() throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        
        User u;
        User u1 = new User("NombreX2", "PassX2");
        User u2 = new User("NombreY2", "PassY2");

        entityManager.persist(u1);
        entityManager.persist(u2);

        assertNotEquals(u1.getId(), u2.getId());
        assertEquals(u1.getId()+1, u2.getId()); //<-- No es necesariamente true
        
        u = entityManager.find(User.class, u1.getId());
        assertEquals(u, u1);
        
        u = entityManager.find(User.class, u2.getId());
        assertEquals(u, u2);
        
        entityManager.getTransaction().commit();
        entityManager.close();
    }
    
    @Test
    public void changePasswordIsUpadate() throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        
        User u = new User("NombreX3", "PassX3");

        entityManager.persist(u);

        long idOriginal = u.getId();

        u.setPassword("PassZ");
        
        //entityManager.persist(u); //Esta linea estaria demas, totalmente innecesaria, ya que u ya esta attached (managed)
        
        assertEquals(idOriginal, u.getId());
        
        User u2 = entityManager.find(User.class, idOriginal);
        assertEquals("PassZ", u2.getPassword());
        assertEquals(u, u2);
        
        entityManager.getTransaction().commit();
        entityManager.close();
    }
    @Test
    public void changeIDIsNoOk() throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        
        User u = new User("NombreX4", "PassX4");

        entityManager.persist(u);
        System.out.println("*** UserTest.changeIDIsNoOk(), u = " + u);
        long id = u.getId(); 
        u.setId(id +1 );
        entityManager.persist(u);  // Aqui no hay exception (eneraria un error interna de HB?)
        System.out.println("*** UserTest.changeIDIsNoOk(), u = " + u);
        try{
        	entityManager.getTransaction().commit();
        	fail("Something is wrong, allows change Identities of a managed entity? ");
        }catch (PersistenceException ex){
        	entityManager.getTransaction().rollback();
        }
        u = entityManager.find(User.class, id);
        assertNull(u);
        System.out.println("*** UserTest.changeIDIsNoOk(), id no encontraria(salto de correlativo?): " + id);
        entityManager.close();
    }
    
    @Test
    public void mergeAsNew()  throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        User u = new User("NombreX5", "PassX5");
        
        List<User> lu1= entityManager.createNativeQuery("select * from User").getResultList();
        entityManager.merge(u);
        entityManager.merge(u);
        entityManager.merge(u);
        List<User> lu2= entityManager.createNativeQuery("select * from User").getResultList();
        assertEquals(lu1.size()+3, lu2.size());

        entityManager.getTransaction().commit();
        entityManager.close();
        
    }

    @Test
    public void mergeBadUsedAsUpdate()  throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User u = new User("NombreX6", "PassX6");
        
        entityManager.getTransaction().begin();
        User u1 = entityManager.merge(u);
        User u2 = entityManager.merge(u);
        User u3 = entityManager.merge(u);
        
        assertNotSame(u1, u);
        assertNotSame(u2, u);
        assertNotSame(u3, u);
        
        List<User> lu1= entityManager.createNativeQuery("select * from User").getResultList();

        u1.setPassword("ModifiedPass1");
        u2.setPassword("ModifiedPass2");
        u3.setPassword("ModifiedPass3");
        User u1m = entityManager.merge(u1);  // Ya que u1, u2, u3 son managed,  
        User u2m = entityManager.merge(u2);  // estos merge estaria de demas!!
        User u3m = entityManager.merge(u3);

        List<User> lu2= entityManager.createNativeQuery("select * from User").getResultList();
        assertEquals(lu1.size(), lu2.size());
        
        assertSame(u1m, u1);
        assertSame(u2m, u2);
        assertSame(u3m, u3);

        User u1mod  = entityManager.find(User.class, u1.getId());
        assertEquals("ModifiedPass1", u1mod.getPassword());
        
        entityManager.getTransaction().commit();
        
        entityManager.close();
        
    }
    
    @Test
    public void thePrecenseOfAQueryCauseExceptionButWhy()  throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User u = new User("NombreX6", "PassX6");
        
        entityManager.getTransaction().begin();
        User u1 = entityManager.merge(u);
        assertNotSame(u1, u);

        // Con la aparicion de esta linea causa esta excepcion:
        //javax.persistence.OptimisticLockException: Row was updated or deleted by another transaction (or unsaved-value mapping was incorrect)
       	//Caused by: org.hibernate.StaleObjectStateException: Row was updated or deleted by another transaction (or unsaved-value mapping was incorrect)
        // la ejecucion de un query hace que 
        List<User> lu2= entityManager.createNativeQuery("select * from User").getResultList();
        
        u.setId(u1.getId());
        u.setPassword("ModifiedPass**");
        entityManager.merge(u);
        assertEquals("ModifiedPass**", u1.getPassword());
        
        entityManager.getTransaction().commit();
        entityManager.close();
        
    }

    @Test
    public void mergeUsedAsUpdate()  throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User u1 = new User("NombreX7", "PassX7");
        User u2 = new User("NombreX8", "PassX8");
        
        entityManager.getTransaction().begin();
        User mu1 = entityManager.merge(u1);
        User mu2 = entityManager.merge(u2);
        assertNotSame(mu1, u1);
        assertNotSame(mu2, u2);

        u1.setId(mu1.getId());
        u1.setPassword("ModifiedPass**");
        entityManager.merge(u1);
        assertEquals("ModifiedPass**", mu1.getPassword());
        assertNotSame(mu1, u1); // A pesar que hizo el merge con el campo ID, u1 no es managed
        
        u1.setId(mu2.getId());   // Ya que no me managed, se puede cambiar el ID para actualizar otra entidad
        entityManager.merge(u1);
        assertEquals("ModifiedPass**", mu2.getPassword());
        assertNotSame(mu2, u1);
        
        u1.setId(mu1.getId());   // Ya que no me managed, se puede cambiar el ID para actualizar otra entidad
        mu1.setPassword("?");
        entityManager.merge(mu1);
        u1.setPassword("ModifiedPassAgain**");
        entityManager.merge(u1);
        assertEquals("ModifiedPassAgain**", mu1.getPassword());
        assertNotSame(mu1, u1);
        
        entityManager.getTransaction().commit();
        
        entityManager.close();
    }
    
    @Test
    public void mergeAsUpdateInSameTransactionWithDetachObjectShouldOk()  throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User u = new User("NombreX", "PassX");
        
        entityManager.getTransaction().begin();
        User u1 = entityManager.merge(u);
        assertNotSame(u1, u);
        
        u.setId(u1.getId());
        u.setPassword("ModifiedPass");
        entityManager.merge(u);
        assertNotSame(u1, u);
        assertEquals(u1.getPassword(), u.getPassword());
        
        u.setPassword("ModifiedPass2");
        User modu = entityManager.merge(u);
        assertNotSame(u1, u);
        assertSame(u1, modu);
        
        entityManager.getTransaction().commit();
        
        entityManager.close();
        
    }

    
    @Test
    public void mergeAsUpdateInNewTransactionWithAttachObjectShouldOk()  throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User u = new User("NombreX", "PassX");
        
        entityManager.getTransaction().begin();
        User u1 = entityManager.merge(u);
        assertNotSame(u1, u);
        entityManager.getTransaction().commit();
        
        entityManager.getTransaction().begin();
        u1.setPassword("ModPass");
        User u2 = entityManager.merge(u1);
        assertSame(u1, u2);
        entityManager.getTransaction().commit();
        
        entityManager.close();
        
    }
    
    @Test
    public void mergeAsUpdateInNewTransactionWithDetachObjectShouldError()  throws Exception {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User u = new User("NombreX", "PassX");
        
        entityManager.getTransaction().begin();
        User u1 = entityManager.merge(u);
        assertNotSame(u1, u);
        entityManager.getTransaction().commit();
        
        entityManager.getTransaction().begin();
        u.setId(u1.getId());
        u.setPassword("ModifiedPass");
        try{
        	entityManager.merge(u);
        	fail("Usualmente debe ser OptimisticLockException, pero en puede depender de la configuracion");
        	entityManager.getTransaction().commit();
        }catch (OptimisticLockException ex){
        	entityManager.getTransaction().rollback();
        }finally {
            entityManager.close();
		}
    }
    
}
