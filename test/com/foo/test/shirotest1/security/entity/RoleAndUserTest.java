package com.foo.test.shirotest1.security.entity;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.OptimisticLockException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.metamodel.EmbeddableType;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.Metamodel;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RoleAndUserTest {
	private EntityManagerFactory emf;

	@Before
    public void beforTest() {
        emf = Persistence.createEntityManagerFactory( "PUTest-ListUserAndRole" );
	}

    @After
    public void afterTest() {
        emf.close();
    }
    
	@Test
    public void twoRolesAndThreeUsers() throws Exception {
        
        Role r1 = new Role("Admin");
        Role r2 = new Role("User");
        User u1 = new User("User1", "Pass1");
        User u2 = new User("User2", "Pass2");
        User u3 = new User("User3", "Pass3");

        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(r1);
        entityManager.persist(r2);
        entityManager.persist(u1);
        entityManager.persist(u2);
        entityManager.persist(u3);

        entityManager.getTransaction().commit();
        
        List<Role> roles = entityManager.createQuery("from Role r", Role.class).getResultList();
        assertTrue(roles.contains(r1));
        assertTrue(roles.contains(r2));
        for (Role role : roles) {
			System.out.println(role.getId() + ":" + role.getName());
		} 
        
        List<User> users = entityManager.createQuery("from User u", User.class).getResultList();
        assertTrue(users.contains(u1));
        assertTrue(users.contains(u2));
        assertTrue(users.contains(u3));
        for (User user : users) {
			System.out.println(user.getId() + ":" + user.getUsername());
		}

        Role r = entityManager.find(Role.class, 1L);
        assertSame(r, r1);
        assertNotNull(r);
        User u = entityManager.find(User.class, 1L);
        assertNotSame(u, u1);
        assertNull(u);
        
        entityManager.close();
    }

	@Test
    public void twoRolesAndThreeUsersRelated() throws Exception {
        
        Role r1 = new Role("Admin");
        Role r2 = new Role("User");
        User u1 = new User("User1", "Pass1"); u1.setRole(r1);
        User u2 = new User("User2", "Pass2"); u2.setRole(r2);
        User u3 = new User("User3", "Pass3"); u3.setRole(r2);
        
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();

        //Since we use CascadeType.PERSIST in the relationship
        //we dont need persist Role first in order to use User
        //entityManager.persist(r1);
        //entityManager.persist(r2);
        entityManager.persist(u1);
        entityManager.persist(u2);
        entityManager.persist(u3);

        entityManager.getTransaction().commit();
        
        List<Role> roles = entityManager.createQuery("from Role r", Role.class).getResultList();
        assertTrue(roles.contains(r1));
        assertTrue(roles.contains(r2));
        for (Role role : roles) {
			System.out.println(role.getId() + ":" + role.getName());
		} 
        
        List<User> users = entityManager.createQuery("from User u", User.class).getResultList();
        assertTrue(users.contains(u1));
        assertTrue(users.contains(u2));
        assertTrue(users.contains(u3));
        for (User user : users) {
			System.out.println(user.getId() + ":" + user.getUsername());
		}

        Role r = entityManager.find(Role.class, 1L);
        assertNotSame(r, r1);
        assertNull(r);
        User u = entityManager.find(User.class, 1L);
        assertSame(u, u1);
        assertNotNull(u);
        
        entityManager.close();
    }

}
