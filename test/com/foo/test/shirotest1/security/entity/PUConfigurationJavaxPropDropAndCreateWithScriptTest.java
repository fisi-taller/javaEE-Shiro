package com.foo.test.shirotest1.security.entity;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PUConfigurationJavaxPropDropAndCreateWithScriptTest {
	private static final String PU_NAME = "PUTest-drop-and-create-with-script";
	private static final String SCHEMA_GEN_ACTION_PROP_NAME = "javax.persistence.schema-generation.database.action";
	private static final String SCHEMA_GEN_ACTION_PROP_VAL = "drop-and-create";
	
	private static EntityManagerFactory emf;

	@BeforeClass
    public static void beforClass() {
		emf = Persistence.createEntityManagerFactory( PU_NAME );
		assertEquals(SCHEMA_GEN_ACTION_PROP_VAL, emf.getProperties().get(SCHEMA_GEN_ACTION_PROP_NAME));
	}
	@AfterClass
    public static void afterClass() {
		emf.close();
	}

	@Before
    public void beforTest() {
		emf = Persistence.createEntityManagerFactory( PU_NAME );
	}

	@After
    public void afterTest() {
		emf.close();
	}

    @Test
    public void withAnyTestShouldDropCreateTheSchemaAndLoadData() throws Exception {
        EntityManager em = emf.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(3, lu.size());

        em.close();
    }
    
    @Test
    public void withAnyTestShouldDropCreateTheSchemaAndLoadDataAndShouldInsertWithTheCorrectSequence() throws Exception {
        EntityManager em = emf.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(3, lu.size());
        
        em.getTransaction().begin();
        User u4 = em.merge(new User("Nom4", "Pass4"));
        User u5 = em.merge(new User("Nom4", "Pass4"));
        assertEquals(4, u4.getId());
        assertEquals(5, u5.getId());
        
        em.getTransaction().commit();
        em.close();
    }

}
