package com.foo.test.shirotest1.security.entity;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PUConfigurationJavaxPropDllGenerationTest {
	private static final String PU_NAME = "PUTest-ddl-generation";
	private static final String SCRIPT_GEN_ACTION_PROP_NAME = "javax.persistence.schema-generation.scripts.action";
	private static final String SCRIPT_GEN_ACTION_PROP_VAL = "drop-and-create";
	
	private static final String CREATE_SCRIPT = "test-resources/sql/generated/create.sql";
	private static final String DROP_SCRIPT = "test-resources/sql/generated/drop.sql";
	
	private static EntityManagerFactory emf;
	private static Date startTime;

	@BeforeClass
    public static void beforClass() {
		startTime = new Date();
		System.out.println("*** beforClass now: " + startTime + ", " + startTime.getTime());
	
        File createFile = new File(CREATE_SCRIPT);
        File dropFile = new File(DROP_SCRIPT);

        if (createFile.exists()) {
        	System.out.println("*** beforClass(), deleted " + createFile.getPath() );
        	createFile.delete();
        }
        if (dropFile.exists()) {
        	System.out.println("*** beforClass(), deleted " + dropFile.getPath() );
        	dropFile.delete();
        }

		emf = Persistence.createEntityManagerFactory( PU_NAME );
		assertEquals(SCRIPT_GEN_ACTION_PROP_VAL, emf.getProperties().get(SCRIPT_GEN_ACTION_PROP_NAME));

	
	}
	@AfterClass
    public static void afterClass() {
		emf.close();
	}

    @Test
    public void theScriptShouldGenerated() throws Exception {
        Date now = new Date();
        System.out.println("*** now: " + now + ", " + now.getTime());

        File createFile = new File(CREATE_SCRIPT);
        File dropFile = new File(DROP_SCRIPT);

        assertTrue(createFile.exists());
        assertTrue(dropFile.exists());
        
        BasicFileAttributes cfAttr = Files.readAttributes(Paths.get(createFile.getAbsolutePath()), BasicFileAttributes.class);
        BasicFileAttributes dfAttr = Files.readAttributes(dropFile.toPath(), BasicFileAttributes.class);
        System.out.println("*** create file:" + createFile.getAbsolutePath() + ", dropFile: " + dropFile.getPath());
        
        System.out.println("*** lastModifiedTime:" + cfAttr.lastModifiedTime() + ", " + cfAttr.lastModifiedTime().toMillis() );
        System.out.println("*** creationTime:" + cfAttr.creationTime() + ", " + cfAttr.creationTime().toMillis());
        assertTrue(laFechaYPosteriorAlYAnteDe(cfAttr.creationTime(), startTime, now));
        assertTrue(laFechaYPosteriorAlYAnteDe(dfAttr.creationTime(), startTime, now));
    }
    
    boolean laDiferenciaEstaDentroDe(Date dt, FileTime ft, int segundos) {
    	return Math.abs(dt.getTime()- ft.toMillis()) < 1000 * segundos;
    }
    
    boolean laFechaYPosteriorAlYAnteDe(FileTime fecha, Date inicio, Date fin) {
    	return fecha.toMillis() > inicio.getTime() && fecha.toMillis() <  fin.getTime(); 
    	//fecha.after(inicio) && fecha.before(fin);
    }

    
}
