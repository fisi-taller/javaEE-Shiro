package com.foo.test.shirotest1.security.entity;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.Metamodel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RoleTest {
	private EntityManagerFactory emf;

	@Before
    public void beforTest() {
        emf = Persistence.createEntityManagerFactory( "PUTest-ListUserAndRole" );
	}

    @After
    public void afterTest() {
        emf.close();
    }
    
    
    @Test
    public void whithRoleNoListedPUShouldThrowException() throws Exception {
    	try{
    		emf = Persistence.createEntityManagerFactory( "shiroJPUTest-Local" );
    	}catch (Exception e){
   			System.out.println("********** RoleTest.whithRoleNoListedPUShouldThrowException() \n" + 
   					"Con las relaciones establecidas, el motivo de excepcion es otro: " + e.getMessage());
   			return;
    	}
    	assertFalse(hasClassInMetaModel(Role.class, emf.getMetamodel()));
    	
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        
        try {
        	entityManager.persist(new Role("Admin"));
        	fail("A pesar que clase Rol no esta en el MetaModel se puede persistir, algo mal...");
        	entityManager.getTransaction().commit();
        }catch(Exception ex){
        	entityManager.getTransaction().rollback();
        }
        entityManager.close();

    }

    private boolean hasClassInMetaModel(Class cl, Metamodel mm) {
    	for (ManagedType obj : mm.getManagedTypes()) {
    		if (obj.getJavaType() == cl)
    			return true;
		} 
    	return false;
    	//return mm.managedType(cl)  != null;
    	//return mm.entity(cl) != null || mm.embeddable(cl) != null;
    }
    
    @Test
    public void whithRoleListedPUShouldOk() throws Exception {
    	emf = Persistence.createEntityManagerFactory( "PUTest-ListUserAndRole" );
    	assertTrue(hasClassInMetaModel(Role.class, emf.getMetamodel()));
    	
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        
        entityManager.persist(new Role("Admin"));

        entityManager.getTransaction().commit();
        entityManager.close();

    }
    
	@Test
    public void afterInsertShouldGetDifferentState() throws Exception {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        
        Role r1 = new Role("Admin");
        assertEquals(0, r1.getId());
        
        entityManager.persist(r1);

        assertNotEquals(0, r1.getId());
        
        Role r = entityManager.find(Role.class, r1.getId());
        assertEquals(r, r1);
        
        entityManager.getTransaction().commit();
        entityManager.close();
    }
    
    @Test
    public void insertTwoRoleShouldGetDifferentID() throws Exception {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        
        Role r1 = new Role("Admin");
        Role r2 = new Role("User");
        
        entityManager.persist(r1);
        entityManager.persist(r2);

        assertNotEquals(r1.getId(), r2.getId());
        
        entityManager.getTransaction().commit();
        entityManager.close();
    }
    
}
