package com.foo.test.shirotest1.security.entity;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PUConfigurationJavaxPropDropAndCreateTest {
	private static final String PU_NAME = "shiroJPUTest-Local-JavaxProp-drop-and-create";
	private static final String SCHEMA_GEN_ACTION_PROP_NAME = "javax.persistence.schema-generation.database.action";
	private static final String SCHEMA_GEN_ACTION_PROP_VAL = "drop-and-create";
	
	
	private static EntityManagerFactory emf;
	private static int recordCount;

	@BeforeClass
    public static void beforClass() {
		emf = Persistence.createEntityManagerFactory( PU_NAME );
		assertEquals(SCHEMA_GEN_ACTION_PROP_VAL, emf.getProperties().get(SCHEMA_GEN_ACTION_PROP_NAME));
		
		recordCount = countRecord();
		assertEquals(0, recordCount);
	}
	@AfterClass
    public static void afterClass() {
		emf.close();
	}

    private static int countRecord()  {
        EntityManager em = emf.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        em.close();
        return lu.size();
    }
	
	
	@Before
    public void beforTest() {
		emf = Persistence.createEntityManagerFactory( PU_NAME );
	}

    @After
    public void afterTest() {
    	emf.close();
    }

    public void insert(EntityManager em, int count) {
        em.getTransaction().begin();
        
        for (int i = 0; i < count; i++)
        	em.persist(new User("Nombre" + i, "Pass" + i));

        em.getTransaction().commit();
    }
    
    
    @Test
    public void insertTwoShouldNotMantainsAfterTest() throws Exception {
        EntityManager em = emf.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(0, lu.size());

        insert(em, 2);

        em.close();
    }


    @Test
    public void insertTwoAgainShouldNotMantainsAfterTest() throws Exception {
        EntityManager em = emf.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(0, lu.size());
        System.out.println("*** count = " + recordCount);

        insert(em, 2);

        em.close();
    }

    @Test
    public void insertOneShouldNotMantainsAfterTest() throws Exception {
        EntityManager em = emf.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(0, lu.size());
        System.out.println("*** count = " + recordCount);

        insert(em, 1);

        em.close();
    }
    
    @Test
    public void withAnyTestShouldDropAndCreateTheSchema() throws Exception {
        EntityManager em = emf.createEntityManager();

        List<User> lu= em.createNativeQuery("select * from User").getResultList();
        assertEquals(0, lu.size());

        em.close();
    }
    
}
