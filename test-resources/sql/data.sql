insert into Role(id, name, enabled, creation) values (1, 'Admin', true, now());
insert into Role(id, name, enabled, creation) values (2, 'User', true, now());

insert into User (id, enabled, password, username, version, role_id) values 
                  (1, 1, 'pass1', 'user1', now(), 1);
                  
insert into User (id, enabled, password, username, version, role_id) values 
                  (2, 1, 'pass2', 'user2', now(), 2);
                  
insert into User (id, enabled, password, username, version, role_id) values 
                  (3, 1, 'pass3', 'user3', now(), 2);                 
                  
insert into hibernate_sequence values (4);


