
    create table hibernate_sequence (
       next_val bigint
    );

    create table Role (
       id bigint not null,
       creation datetime not null,
       enabled bit not null,
       name varchar(50) not null,
       primary key (id)
    );
    
    create table User (
       id bigint not null,
        enabled bit not null,
        password varchar(255) not null,
        username varchar(50) not null,
        version datetime not null,
        role_id bigint,
        primary key (id)
    );

    alter table User 
       add constraint FK84qlpfci484r1luck11eno6ec 
       foreign key (role_id) 
       references Role (id);
    
