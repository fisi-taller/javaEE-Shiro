package com.foo.test.shirotest1.security.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
public class User implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue //(strategy = GenerationType.IDENTITY) Cuatros valores: AUTO IDENTITY TABLE SEQUENCE
	@Column(name = "id", nullable = false, updatable = false)
	private long id;

	@NotNull
	@Column(name = "username", length = 50, nullable = false)
	private String username;

	@NotNull
	@Column(name = "password", nullable = false)
	private String password;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	private Role role;
	
	@Column(name = "enabled", nullable = false)
	private Boolean enabled;

	@Version
	@Column(name = "version", nullable = false)
	private Timestamp version;
	
    public User() {
        this("", "", Boolean.TRUE);
    }

    public User(long id, String username, String password) {
        this(username, password, Boolean.TRUE);
        this.setId(id);
    }

    public User(String username, String password) {
        this(username, password, Boolean.TRUE);
    }

    public User(String username, String password, Boolean enabled) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }

	public long getId() {
		return id;
	}
    public void setId(long id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
	
	public Object getPassword() {
		return password;
	}

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
		
	}
    
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    public Timestamp getVersion() {
        return version;
    }

    public void setVersion(Timestamp version) {
        this.version = version;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (int) (this.getId() ^ (this.getId() >>> 32));
        hash = 67 * hash + Objects.hashCode(this.getUsername());
        hash = 67 * hash + Objects.hashCode(this.getPassword());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.getId() != other.getId()) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + getId() + ", username=" + getUsername() + ", password=" + getPassword() + ", enabled=" + getEnabled()  + "}";
    }
	
}
