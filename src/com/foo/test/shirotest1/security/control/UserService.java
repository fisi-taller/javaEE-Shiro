package com.foo.test.shirotest1.security.control;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;

import com.foo.test.shirotest1.security.entity.User;


@ApplicationScoped
public class UserService {
	

    List<User> users = new ArrayList<User>();
    public UserService() {
    	users.add(new User(1, "User1", "Pass1"));
    	users.add(new User(2, "User2", "Pass2"));
    	users.add(new User(3, "User3", "Pass3"));
	}

	public User findById(long id) {
		return users.stream().filter( u -> u.getId() == id).collect(Collectors.toList()).get(0);
	}
	 
	public List<User> findAll() {
		return users;
	}

	public User findByUsername(String username) {
		return users.stream().filter( u -> u.getUsername().equalsIgnoreCase(username)).collect(Collectors.toList()).get(0);
	}

	public User save(User user) {
		boolean found = false;
		for (int i = 0; i < users.size(); i++ ){
			User u = users.get(i);
			if (u.getId() == user.getId()) {
				users.set(i, user);
				found = true;
				break;
			}
		}
		if (!found)
			users.add(user);
		return user;
	}

	public void delete(long id) {
		boolean found = false;
		for (int i = 0; i < users.size(); i++ ){
			User u = users.get(i);
			if (u.getId() == id) {
				users.remove(i);
				found = true;
				break;
			}
		}
		if (!found)
			throw new RuntimeException("Usuario con id no encontrado: " + id);
		return;
	}
	
	
	
	
	
}